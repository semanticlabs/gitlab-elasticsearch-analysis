#!/bin/bash

for i in $(seq 0 20)
do
  printf -v i "%02d" "$i"
  bash reindex.sh test_"$i"
done

bash reindex.sh test_combined
