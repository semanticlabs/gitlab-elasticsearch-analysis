#!/bin/bash

for i in $(seq 0 20)
do
  printf -v i "%02d" "$i"
  echo "test_$i"
  curl -s "http://localhost:9200/gitlab-test_$i/_stats" | jq '._all.primaries.store,._all.primaries.docs'
done

echo "test_combined"
curl -s "http://localhost:9200/gitlab-test_combined/_stats" | jq '._all.primaries.store,._all.primaries.docs'
