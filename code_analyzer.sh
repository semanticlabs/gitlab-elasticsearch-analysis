#!/bin/bash

curl -s -XGET "http://localhost:9200/gitlab-$1/_analyze" -H 'Content-Type: application/json' -d@- <<EOF | jq .
{
  "analyzer": "code_analyzer",
  "text": "$2"
}
EOF
