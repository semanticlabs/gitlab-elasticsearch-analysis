#!/bin/bash

curl -s -XDELETE "http://localhost:9200/gitlab-$1" | jq .
curl -s -XPOST "http://localhost:9200/_refresh" | jq .
curl -s -XPOST "http://localhost:9200/_flush" | jq .

curl -s -XPUT "http://localhost:9200/gitlab-$1" -H 'Content-Type: application/json' -d @"$1.json" | jq .

curl -s -XPOST "http://localhost:9200/_reindex?wait_for_completion=true" -H 'Content-Type: application/json' -d"
{
  \"source\": {
    \"index\": \"gitlab-production\"
  },
  \"dest\": {
    \"index\": \"gitlab-$1\"
  }
}" | jq .

curl -s -XPOST "http://localhost:9200/gitlab-$1/_forcemerge?max_num_segments=1" | jq .

echo "$1:"
curl -s "http://localhost:9200/gitlab-$1/_stats" | jq '._all.primaries.store,._all.primaries.docs'
