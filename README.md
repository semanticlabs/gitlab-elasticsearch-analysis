# GitLab Elasticsearch Tuning

This is a collection of scripts and test scenarios to improve the storage efficieny and indexing performance of GitLab's Elasticsearch cluster.

## Prepare Elasticsearch production index

Ensure that you have an Elasticsearch index based on the current GitLab mapping available at http://localhost:9200/gitlab-production.

## Create index per test scenario

The bash scripts depend on the following tools:

- [curl](https://curl.haxx.se/)
- [jq](https://stedolan.github.io/jq/)

So make sure to have them installed. Then create an index per test scenario from the `gitlab-production` index:

```shell script
./run_all.sh
```

This can take a while depending on the size of the index. Once the indices have been created, you can get the stats for each index (storage size, number of docs) by running the `get_stats.sh` script:

```shell script
./get_stats.sh
```

The test scenarios are described in this [Google document].

## Testing the code analyzer

The script `code_analyzer.sh` can be used to determine which tokens are emitted by the `code_analyzer` of a specific test scenario:

```shell script
./code_analyzer.sh test_00 "console.log('test')"
```

The output of the script is the response of Elasticsearch's [_analyze endpoint](https://www.elastic.co/guide/en/elasticsearch/reference/6.8/indices-analyze.html).

[Google document]: https://docs.google.com/document/d/15r5LIrxTQxxQCpptm1K1Xui0YfCdFRDRmO4-CuUof-8/edit?usp=sharing
